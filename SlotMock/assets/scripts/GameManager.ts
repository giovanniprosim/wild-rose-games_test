const { ccclass, property } = cc._decorator;

import MachinePattern from './MachinePattern';

@ccclass
export default class GameManager extends cc.Component {
  @property(cc.Node)
  machine = null;

  @property({ type: cc.AudioClip })
  audioClick = null;

  private block = false;

  private result = null;

  start(): void {
    this.machine.getComponent('Machine').createMachine();
  }

  update(): void {
    if (this.block && this.result != null) {
      this.informStop();
      this.result = null;
    }
  }

  click(): void {
    cc.audioEngine.playEffect(this.audioClick, false);

    if (this.machine.getComponent('Machine').spinning === false) {
      this.block = false;
      // Giovanni
      // Ao inicializar o giro da máquina, reseta as animações
      this.machine.getComponent('Machine').showGlowEffectsAtList()
      this.machine.getComponent('Machine').spin();
      this.requestResult();
    } else if (!this.block) {
      this.block = true;
      this.machine.getComponent('Machine').lock();
    }
  }

  async requestResult(): Promise<void> {
    this.result = null;
    this.result = await this.getAnswer();
  }

  getAnswer(): Promise<Array<Array<number>>> {
    const slotResult = this.raffleResult();
    return new Promise<Array<Array<number>>>(resolve => {
      setTimeout(() => {
        resolve(slotResult);
      }, 1000 + 500 * Math.random());
    });
  }

  raffleResult(): Array<Array<number>> {
    // Giovanni
    // Sorteia o padrão da máquina e com o mesmo, monta
    // monta o resultado final que será repassado para até
    // chegar ao Tile.ts
    const result = []

    const patternName = MachinePattern.rafflePattern()
    const winnerLines = MachinePattern.pattern[patternName].lines
    console.log("PATTERN: ", patternName)

    const linesInView = 3
    
    // Giovanni
    // A montagem do resultado segue o seguinte padrão:
    // result: [
    //   [2, null, null],
    //   [2, null, null],
    //   [2, null, null],
    //   [2, null, null],
    //   [2, null, null],
    // ]
    // No exemplo acima, apenaso padrão sorteado foi o ONE,
    // ou seja, apenas uma será igual e os tiles terão o
    // sprite respectivo ao de indíce 2. Os com null terão seus
    // sprites sorteados na função setRandom() do Tile.ts
    for(let i = 0; i < linesInView; i++) {
      const textureID =   i < winnerLines ? Math.floor(Math.random() * 30) : null
      for(let j = 0; j < this.machine.getComponent('Machine').numberOfReels; j++) {
        if(!result[j]) result[j] = []
        result[j][i] = textureID
      }
    }
    return result
  }

  informStop(): void {
    const resultRelayed = [...this.result];
    this.machine.getComponent('Machine').stop(resultRelayed);
  }
}

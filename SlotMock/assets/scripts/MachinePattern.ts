const { ccclass } = cc._decorator;

@ccclass
export default class MachinePattern extends cc.Component {

	static pattern = {
		RANDOM: { percent: .5},
		ONE: { percent: .33, lines: 1 },
		TWO: { percent: .1, lines: 2 },
		THREE: { percent: .07, lines: 3 },
	};

	static rafflePattern(): string {
		// Giovanni
		// Faz o sorteio de qual pattern será escolhido, levando em consideração sua chance de
		// ser escolhido, representado pelo 'percent', dentre cada objeto do pattern.
		const raffleValue = Math.random();

		let nextMinPercent = 0;

		const machinePatternKeys = Object.keys(this.pattern);

		// Giovanni
		// Essa função vai passar por todas as chaves do objeto pattern, verificando dentro de
		// qual intervalo o raffleValue se encaixa
		return machinePatternKeys.find(key => {
			const currentMinPercent = nextMinPercent;
			const currentMaxPercent = currentMinPercent + this.pattern[key].percent;
			nextMinPercent = currentMaxPercent;
			return currentMinPercent <= raffleValue && currentMaxPercent > raffleValue;
		});
	}

}

const { ccclass } = cc._decorator;

@ccclass
export default class GlowEffectGroup extends cc.Component {
    
	public showEffectsByListId(showList: Array<number> = []): void {
		// Giovanni
		// Função feita para ativar as animações referentes aos IDs passados no showList,
		// os IDs que não forem referenciados, não serão animados.
		// Obs: no caso de chamar a função sem passar parâmetros, todas as animações serão
		// inativadas, como o comportamento padrão.
		let glowEffects = this.node.children
		glowEffects.forEach((ge, id) => {
			const idToShow = showList.findIndex(showId => id == showId)
			if(idToShow >= 0) { //show current glow effect
				ge.active = true
				showList.splice(idToShow,1)
			}
			else{
				ge.active = false
			}
		})

		this.node.active = true
	}

}

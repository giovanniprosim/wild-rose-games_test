import Aux from '../SlotEnum';

const { ccclass, property } = cc._decorator;

@ccclass
export default class Machine extends cc.Component {
  @property(cc.Node)
  public button: cc.Node = null;

  @property(cc.Node)
  public glowEffectsGroup: cc.Node = null;

  @property(cc.Prefab)
  public _reelPrefab = null;

  @property({ type: cc.Prefab })
  get reelPrefab(): cc.Prefab {
    return this._reelPrefab;
  }

  set reelPrefab(newPrefab: cc.Prefab) {
    this._reelPrefab = newPrefab;
    this.node.removeAllChildren();

    if (newPrefab !== null) {
      this.createMachine();
    }
  }

  @property({ type: cc.Integer })
  public _numberOfReels = 3;

  @property({ type: cc.Integer, range: [3, 6], slide: true })
  get numberOfReels(): number {
    return this._numberOfReels;
  }

  set numberOfReels(newNumber: number) {
    this._numberOfReels = newNumber;

    if (this.reelPrefab !== null) {
      this.createMachine();
    }
  }

  private reels = [];

  public spinning = false;

  createMachine(): void {
    this.node.destroyAllChildren();
    this.reels = [];

    let newReel: cc.Node;
    for (let i = 0; i < this.numberOfReels; i += 1) {
      newReel = cc.instantiate(this.reelPrefab);
      this.node.addChild(newReel);
      this.reels[i] = newReel;

      const reelScript = newReel.getComponent('Reel');
      reelScript.shuffle();
      reelScript.reelAnchor.getComponent(cc.Layout).enabled = false;
    }

    this.node.getComponent(cc.Widget).updateAlignment();
  }

  spin(): void {
    this.spinning = true;
    this.button.getChildByName('Label').getComponent(cc.Label).string = 'STOP';

    for (let i = 0; i < this.numberOfReels; i += 1) {
      const theReel = this.reels[i].getComponent('Reel');

      if (i % 2) {
        theReel.spinDirection = Aux.Direction.Down;
      } else {
        theReel.spinDirection = Aux.Direction.Up;
      }

      theReel.doSpin(0.03 * i);
    }
  }

  lock(): void {
    this.button.getComponent(cc.Button).interactable = false;
  }

  stop(result: Array<Array<number>> = null): void {
    setTimeout(() => {
      this.spinning = false;
      this.button.getComponent(cc.Button).interactable = true;
      this.button.getChildByName('Label').getComponent(cc.Label).string = 'SPIN';
      
      // Giovanni
      // Após terminar o sortei, tendo o resultado estabelecido pelo padrão,
      // consigo saber quais as linhas que deverão ser animadas.
      let listOfGlowEffectLId = []
      // Fazendo um forEach no indice 0 do resultado, consigo verificar em qual
      // das 3 linhas terei que iniciar a animação, pois se algum dos valores do
      // array for igual a null, saberei que foi aleatório e que aquela linha não precisará
      // ter animação.
      result[0].forEach((r,i) => r !== null && listOfGlowEffectLId.push(i))
      // A função showEffectsByListId recebe um array com o indice das linhas que
      // precisam ser animadas. Exemplo, se passar como parâmetro o array [0,2], as
      // linhas animadas serão a 1ª e a última (considerando que tem apenas 3 visíveis).
      this.glowEffectsGroup.getComponent('GlowEffectGroup').showEffectsByListId(listOfGlowEffectLId)
    }, 2500);

    const rngMod = Math.random() / 2;
    for (let i = 0; i < this.numberOfReels; i += 1) {
      const spinDelay = i < 2 + rngMod ? i / 4 : rngMod * (i - 2) + i / 4;
      const theReel = this.reels[i].getComponent('Reel');

      setTimeout(() => {
        theReel.readyStop([...result[i]]);
      }, spinDelay * 1000);
    }
  }

  showGlowEffectsAtList(showList: Array<number> = []): void {
    this.glowEffectsGroup.getComponent('GlowEffectGroup').showEffectsByListId(showList)
  }
}
